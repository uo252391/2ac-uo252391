Tama�o de la palabra de memoria = 16 bits

N�mero de bits de direcciones de memoria = 12

Cach� unificada (un solo nivel)

N�mero de bloques de cache = 8

N�mero de palabras por bloque = 4

Correspondencia asociativa por conjuntos de 8 v�as

Algoritmo de reemplazo LRU

Algoritmo de escritura write-back

Tam. mem. principal: 8 KiB
Bloques de mem. principal: 2048
Tam. mem. cache: 64 bytes
Fallos MP:
	00000D5F
	00000D60
	00000EE7
	00000EE8
Tasa de aciertos: 65%
Bloques de MP reemplazados:
	Direccion    -  Bloque en MP
	000002E0  -  B8h
	000008B7  -  22Dh
Correspondencia		Palabras por bloque	N� conjuntos	N� vias	Aciertos	Fallos
Directa				4		8		1	65%	35%
Totalmente Asociativa		4		1		8	65%	35%
Asociativa por conjuntos		4		2		4	65%	35%