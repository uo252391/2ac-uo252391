#include <atc/linmem.h>

void print_virtual_physical_pte(void *virtual_addr, char *title)
{
        unsigned int physical_addr;
        unsigned int pte;
        unsigned int flags_vp;

        // Print the title
        printf("\n%s\n", title);

        // Retrieve the entry in the page table for the virtual address
        if (get_pte(virtual_addr, &pte))
        {
                perror("Linmem module error");
                return;
        }

        // Is there PTE?
        if (pte)
        { // OK
                // Store the flags associated with the memory page
                flags_vp =pte & 0x000003FF;

                // Calculate the memory physical address
                physical_addr =pte & 0xFFFFF000;
                printf("Virtual address: \t%.8Xh\n"
                                "Page Table Entry:\t%.8Xh\n"
                                "Physical Address:\t%.8Xh\n"
				 "Flags Virtual Page:\t%.3Xh\n",
                                (unsigned int)virtual_addr, pte, physical_addr, flags_vp);
        }
        else
        {
                fprintf(stderr, "Page %.5Xh does not have a page table entry\n",
                        (unsigned int)virtual_addr >> 12);
        }
}





print_virtual_physical_pte(&var2, "Local variable\n-----------------");
