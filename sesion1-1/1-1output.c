#include <stdio.h>

int main()
{
    int i = 3;
    float f = 2.4;
    char * s = "Content";

    // Print the content of i
	printf("Contenido de i: %d", i);
    // Print the content of f
	printf("Contenido de f: %f", f);
    // Print the content of the string
	printf("Contenido de s: %p", s);

    return 0;
}
