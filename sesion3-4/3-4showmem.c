// This source file must be compiled with the following command:
//   gcc -Wall 3-4showmem.c 3-4printvm_lib.c -o 3-4showmem -lmem

#include <stdio.h>

// To use our "library"
extern void print_virtual_physical_pte(void *, char *);

int variable; // Global variable

int main(void)
{
	void *address1;
	void *address2; 


	// Memory Address 1 initialitation
	address1 = (void *) ???????? ;

	// Memory Address 2 initialitation
	address2 = (void *) ???????? ;

	print_virtual_physical_pte((void *)address1, "\nKernel Area Address 1\n"
												"------------------");
	print_virtual_physical_pte((void *)address2, "\nKernel Area Address 2\n"
												"------------------");

	print_virtual_physical_pte((void *)&variable, "\nUser Area Address 3\n"
												"------------------");

	printf("\n---- Press [ENTER] to continue");
	getchar();

	return 0;
}
